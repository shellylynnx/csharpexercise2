﻿using System;

namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>
    public static decimal[] GetArray()
    {
      var arr = new decimal[10];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)0.49, (decimal)149.99);
      }

      return arr;
    }

    /// <summary>
    /// returns an array of random numbers with the specified size, min and max values
    /// </summary>
    /// <param name="size"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static decimal[] GetArray(int size, decimal min, decimal max)
    {
      var arr = new decimal[size];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween(min, max);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    /// <param name="arr"></param>
    public static void OutputArray(decimal[] arr)
    {
      foreach (var a in arr)
      {
        Console.WriteLine(string.Format("{0:C}", a));
      }
    }

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal AverageArrayValue(decimal[] arr)
    {
      var sum = (decimal)0;

      foreach (var a in arr)
      {
        sum += a;
      }

      return sum / arr.Length;
    }

    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal MaxArrayValue(decimal[] arr)
    {
      var maxVal = (decimal)0;

      foreach (var a in arr)
      {
        if (a > maxVal)
        {
          maxVal = a;
        }
      }

      return maxVal;
    }

    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal MinArrayValue(decimal[] arr)
    {
      var minVal = arr[0];

      foreach (var a in arr)
      {
        if (a < minVal)
        {
          minVal = a;
        }
      }

      return minVal;
    }

    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    /// <param name="arr"></param>
    public static void SortArrayAsc(decimal[] arr)
    {
      //bubble sort, don't really expect to see this
      for (var i = 0; i < arr.Length; i++)
      {
        //loop through the entire array
        for (var j = 0; j < arr.Length - 1; j++)
        {
          //loop through the array's "next" elements

          //if the item at j is greater than the next item, switch the items around
          if (arr[j] > arr[j + 1])
          {
            var x = arr[j + 1];

            arr[j + 1] = arr[j];
            arr[j] = x;
          }
        }
      }
    }

    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
