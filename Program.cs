﻿using System;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("enter array size");

      var size = Convert.ToInt32(Console.ReadLine());

      Console.WriteLine("enter array min value");

      var min = Convert.ToDecimal(Console.ReadLine());

      Console.WriteLine("enter array max value");

      var max = Convert.ToDecimal(Console.ReadLine());

      //var arr = ArrayFactory.GetArray();
      var arr = ArrayFactory.GetArray(size, min, max);

      Console.WriteLine("**** OutputArray ****");
      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n**** AverageArrayValue ****");
      var result = ArrayFactory.AverageArrayValue(arr);
      Console.WriteLine(string.Format("{0:C}", result));

      Console.WriteLine("\r\n**** MinArrayValue ****");
      result = ArrayFactory.MinArrayValue(arr);
      Console.WriteLine(string.Format("{0:C}", result));

      Console.WriteLine("\r\n**** MaxArrayValue ****");
      result = ArrayFactory.MaxArrayValue(arr);
      Console.WriteLine(string.Format("{0:C}", result));

      Console.WriteLine("\r\n**** SortArrayAsc ****");
      ArrayFactory.SortArrayAsc(arr);
      ArrayFactory.OutputArray(arr);

      Console.Read();
    }
  }
}
